name             'gm_install_pdsh'
description      'Installs/Configures gm_install_pdsh'
maintainer       'General Motors'
maintainer_email 'rick.connelly@gm.com'
issues_url 'https://gm-com.socialcast.com/groups/186552-chef' if respond_to?(:issues_url)
source_url 'https://bitbucket.gm.com/projects/HAD125874'
chef_version '>= 12'
supports 'oracle'
license          'All rights reserved'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
