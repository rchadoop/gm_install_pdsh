# gm_install_pdsh Cookbook

-   This cookbook installs the pdsh utility

## Requirements

- Valid chef environment

### Platforms

- OEL 6.9 (Probably is not specific that just that, but only tested here)

### Chef

- Chef 12.0 or later

### Cookbooks

- No dependencies on other cookbooks

## Attributes
- At present, no attributes are included or needed

## Usage

This cookbook is comprised of 1 recipe:   

   *  default.rb    
      This recipe defines the package to install.
   
     
## License and Authors

Authors: Rick Connelly & James Mullaney

