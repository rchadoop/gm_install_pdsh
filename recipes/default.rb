#
# Cookbook Name:: gm_install_pdsh
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
template '/etc/yum.repos.d/GMIT.repo' do
  source 'gmit.erb'
  mode '0644'
  owner 'root'
  group 'root'
end

package 'pdsh'
